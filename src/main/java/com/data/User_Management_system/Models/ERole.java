package com.data.User_Management_system.Models;

public enum ERole {

	ROLE_USER,
    ROLE_ADMIN
}
